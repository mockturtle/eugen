# Simple project description format

At the moment (January 2020) `eugen` has a single input plugin: a parser for the simple project description format described here. An example of complete file can be found [here](doc/examples/example_project.simple).  Syntax highlighting files for `emacs` and `gedit` can be found [here](Syntax-Highlight).

## Informal description 

The format takes inspiration from both XML and LaTeX.  As in XML the file is made of "nodes" where every node has a *tag*, *attributes* and *content* (that can include other nodes); as in LaTeX, there are no closing tags, a node is implicitely closed when a new one is encountered. The format is line-based: first the file is split into lines, every line classified according to its type and the resulting sequence of classified lines parsed (with the finite state automata described in [this figure](doc/state-machine.svg), if you are interested in this kind of details)

The line types are
* **comment lines** Their first non-space character is `#`.  They are completely ignored
* **empty lines** they have no characters or only spaces and tabs. They are ignored everywhere by inside the node content
* **header lines** they have the format `[id]` (an identifier in square brackets) and mark the beginning of a node. Example: `[wp]`, `[task]`. Spaces are ignored
* **attribute lines** the have the format `id : value`, that is, an identifier (the attribute name) followed by a colon in turn followed by the attribute value. The spaces around the colon are ignored, bu **not any space in front of the identifier**. Let me emphasize this: in order to be classified as attribute line, **the identifier must begin at the beginning of the line**. Why this constraint? Because it is reasonable to expect that sometimes we could want to write a content line that begins with a name and a colon. In this case we can avoid ambiguities by adding a space in front of the content line
* **content lines** They are lines that do not belong to any of the categories above. This "negative definition" can look strange, but it is how actually they are recognized in the code.

A node description has the following 3-section structure

1. The *header* line
2. Any number (including zero) of *attribute* lines
3. Any number (including zero) of *content* lines

Comment lines are ignored everywhere, empty lines are ignored everywhere but inside the content section

## Formal description with ABNF

This is the syntax in ABNF form (almost, see `text-line`)

```ABNF
   file       = 1*node
   node       = *skipped header *skipped attributes content
   header     = *WSP "[" *WSP id *WSP  "]"  *WSP EOL
   attribute  = id *WSP ":" *BODY-CHAR EOL
   
   attributes = attribute *(*skipped attribute)
   content    = *(text-line / comment)
   
   skipped    = comment / empty-line
   comment    = *WSP # *BODY-CHAR EOL
   empty-line = *WSP EOL
   BODY-CHAR  = VCHAR \ CR \ LF
   EOL        = CR / LF / CRLF
   id         = ALPHA *(ALPHA / DIGIT / "_")
   text-line  = line that does not match neither header, nor attribute, nor comment
```

In plain English:
* The file is a sequence of "nodes"
* Every node has 
  *  A *header* line
  *  An optional *attribute* section
  *  An optional *content* section
* The header line has as first non-whitespace character "[".  The identifier between "[]" identifies the type of node
* An attribute line begins with an identifier (with NO whitespace at the beginning!) followed by a colon ":".  The text after the colon is the attribute value. Whitespaces on both sides of the colon are ignored
* The content section is made of text lines that are defined by difference: they do not satisfy neither the syntax of header, nor of attributes.
* *Comment lines* have as first non-whitespace character "#"
* *Empty lines* are ignored everywhere but in the content section (where they are part of the content)


## Tags and their relationships

The order of tags is quite rigid

* The first tag is *metadata* that holds as attributes some global project data
* After the metadata one or more *partner* nodes follow
* After the partner nodes the *wp* nodes follow, inside a wp node we find
  * A sequence of *task* nodes followed by
  * A sequence of *deliverable* nodes
* After all the wp nodes a list of *milestone* nodes follow

Note that conceptually the *task* and *deliverable* nodes are considered as children of the corresponding *wp*.

## Attributes

### Attributes common to all nodes 

Few attributes are common to most nodes (with the exception of *metadata*)
* **label** the value must be an *identifier* and it must be unique for all the project (no two entities can have the same label).  It is used in several places in order to refer to other nodes and their attributes.  For example, if *mgt* is the label of the Management WP, *mgt.end* is the ending date of the WP.
* **name** is the *name* of the entity (WP, Task, Deliverable, Partner or Milestone)
* **short** is the *short name* of the entity.  It can be used in those places where there is not much space

### WP and Task attributes

Other attributes are specific of "action nodes," that is *WPs* and *tasks*
* **leader** used in WPs and Tasks it is the label of the partner who leads the WP/task
* **begin**, **end**, **duration** specify the starting and ending dates (or duration) of a WP or a Task.  *end* and *duration* are mutually exclusive (one and only one must be given), the not given attribute is derived from *begin* and the given one (that is, if *begin* and *end* are specified, *duration* is set to *end-begin*, similarly if *begin* and *duration* are specified.)
* **effort** This is for tasks only (the WP values are derived from the task)

### Timed nodes (deliverable and milestone) attributes
* **when** The time when the deliverable or the milestone is expected.  Deliverable can specify multiple times separated by semicolons ";" (this is useful, for example, for periodic deliverables).  If missing,
  * In a deliverable the ending time of the associated task is used
  * In a milestone that depends on a deliverable, the time of the deliverable (that cannot be a periodic deliverable) is used

### Deliverable attributes
* **milestones** a list of labels (separated by spaces or commas) of milestones that depend on this deliverable
* **dissemination**  It can assume values "public", "confidential" or "classified"
* **type** it can assume the values "report," "demo," "dissemination," or "other"
* **tasks** a list of labels (separated by spaces or commas) of the tasks that deliver this deliverable

If a deliverable has no *when* attribute, its delivering date defaults at the end of its delivering tasks.

## Times

WP, task, deliverable and milestones have attributes that represent time (*begin*, *end* and *duration* for Task and WPs, *when* for deliverables and milestones). Times are expressed in months and they can be expressed in symbolic form, for example
```
[development]
begin: requirement.end
```

could be used to specify that the WP *development* begins after *requirement* ends. As another example,
```
[wp]
end: max(task1.end, task2.end, task3.end)
```
could be used to specify that `wp` ends when the last of its tasks ends.

The expressions that can be used in time and duration attributes need to adhere to the following rules
* They have the same syntax of usual expressions, with usual precedence
* The variable names are of the form `<label>.<attr>` where `<label>` is the label of a node, while `<attr>` is an attribute name; for example `management.begin` is the beginning date of a WP with label `management`
