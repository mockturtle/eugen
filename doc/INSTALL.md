# How do I install eugen?

At the moment (end January 2020) there is no binary distribution and you need to compile it by yourself.  The program uses a GPR project file. Just put yourself in the top directory (where the file `eugen.gpr` is) and do

```
   gprbuild -P eugen.gpr
```
You'll find the executable `eugen` in `obj/`, just move it where your shell will find it.

# Requirements

You need
* An Ada 2012 compiler

I think that nothing else is required.

If you want to change the [default macros for the `latex` plugin](src/Processor/LaTeX/eugen_macros.tex) you'll need Ruby and run `make` using the Makefile in the top directory (where `eugen.gpr` is).

### My own environment

I work on Linux (Ubuntu e Archie) and the release of my `gprbuild` is *(20190517) (x86_64-pc-linux-gnu)*.  I did not check with other environments, but I do not expect any problems, but let me know.
