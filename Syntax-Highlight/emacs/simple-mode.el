

(setq semplice-highlights
      '(("^\\[ *[a-zA-Z0-9]+ *\\]" . font-lock-function-name-face)
        ("^[a-zA-Z][a-zA-Z0-9_]* *:" . font-lock-preprocessor-face)
        ("^[a-zA-Z][a-zA-Z0-9_]* *:\\(.*\\)$" . (1 font-lock-constant-face))
	("^ *#.*$" . font-lock-comment-face)
	("\\\\[a-zA-Z][a-zA-Z0-9_]*" . font-lock-variable-name-face)
	("{[^{}]*}" . font-lock-variable-name-face)
	))

(define-derived-mode semplice-mode fundamental-mode "semplice"
  "major mode for editing semplice language code."
  (setq font-lock-defaults '(semplice-highlights)))

(add-to-list 'auto-mode-alist '("\\.simple$" . semplice-mode))
