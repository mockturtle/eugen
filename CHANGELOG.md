# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.0.2] - 2020-01-29
### Changed
- Removed the automatic update of the package with the default macro definitions for the `latex` plugin.  The reason is that 
  - said macros do not change often and 
  - `gprbuild` would call the converter every time, even if it was not needed. 
  The problem here is not efficiency, but the fact that the update is done by a Ruby script and this would require Ruby installed. Therefore, the small benefit of automating something that is done very rarely would cost serious portability problems; it is not worth it.

## [0.0.1] - 2020-01-28
### Changed
- First public release.  It is still rough, but it can be usable