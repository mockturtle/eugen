pragma Ada_2012;
with Tokenize.Token_Vectors;

package body Plugins is

   ------------------------
   -- Parse_Plugin_Specs --
   ------------------------

    procedure Parse_Plugin_Specs
         (Input                 : String;
          Plugin_Name           : out Plugin_Id;
          Parameters            : out Plugins.Parameter_Map;
          Plugin_Name_Separator : Character := ':';
          Pair_Separator        : Character := ';';
          Value_Separator       : Character := '=';
          Default_Value         : String := "")
   is
      use Tokenize;

      procedure Parse_Params (Result : out Plugins.Parameter_Maps.Map;
                              Input  : Unbounded_String)
      is
         use Tokenize.Token_Vectors;

         Pairs : constant Token_Vectors.Vector :=
                   To_Vector (Split (To_Be_Splitted    => To_String (Input),
                                     Separator         => Pair_Separator,
                                     Collate_Separator => True));
      begin
         for P of Pairs loop
            declare
               Name  : Unbounded_String;
               Value : Unbounded_String;
            begin
               Head_And_Tail (To_Be_Splitted => P,
                              Separator      => Value_Separator,
                              Head           => Name,
                              Tail           => Value,
                              Trimming       => Both,
                              Default_Tail   => Default_Value);

               Result.Include (Key      => To_String (Name),
                               New_Item => To_String (Value));

            end;
         end loop;
      end Parse_Params;

      function To_Name (X : Unbounded_String) return Plugin_Id
      is (To_Name (To_String (X)));

      Head : Unbounded_String;
      Tail : Unbounded_String;
   begin
      Head_And_Tail (To_Be_Splitted => Input,
                     Separator      => Plugin_Name_Separator,
                     Head           => Head,
                     Tail           => Tail,
                     Trimming       => Both);

      Plugin_Name := To_Name (Head);

      Parse_Params (Parameters, Tail);
   end Parse_Plugin_Specs;

end Plugins;
