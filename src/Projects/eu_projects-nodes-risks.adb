with Ada.Finalization; use Ada.Finalization;
package body EU_Projects.Nodes.Risks is

   --------------
   -- New_Risk --
   --------------

   function New_Risk
     (Label           : Risk_Label;
      Description     : Rich_Text.Core.Rich_Text_Type;
      Countermeasures : Rich_Text.Core.Rich_Text_Type;
      Severity        : Risk_Severity;
      Likeness        : Risk_Likeness)
      return Risk_Access
   is
   begin
      return new Risk_Descriptor'(Controlled with
                                    Label          => Node_Label (Label),
                                  Class          => Risk_Node,
                                  Name           => To_Name(""),
                                  Short_Name     => To_Name(""),
                                  Index          => No_Index,
                                  Description    => Description,
                                  Countemeasures => Countermeasures,
                                  Severity       => Severity,
                                  Likeness       => Likeness,
                                  Attributes     => <>);
   end New_Risk;



   -----------
   -- Label --
   -----------

   function Label (Item : Risk_Descriptor) return Risk_Label is
   begin
      return Risk_Label (Item.Label);
   end Label;



   --------------
   -- Severity --
   --------------

   function Severity (Item : Risk_Descriptor) return Risk_Severity is
   begin
      return Item.Severity;
   end Severity;

   --------------
   -- Likeness --
   --------------

   function Likeness (Item : Risk_Descriptor) return Risk_Likeness is
   begin
      return Item.Likeness;
   end Likeness;
end EU_Projects.Nodes.Risks;
