with Plugins;
with EU_Projects.Projects;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;


package Project_Processor.Parsers is
   subtype Parser_Parameter is Plugins.Parameter_Map;
   subtype Parser_Parameter_access is Plugins.Parameter_Map_Access;

   type Parser_ID is private;

   No_Parser : constant Parser_ID;

   function "<" (X, Y : Parser_ID) return Boolean;

   function To_Id (X : String) return Parser_ID;

   function Find_Parser (File_Extension : String) return Parser_ID;

   function Parse_Project (Input      : String;
                           Format     : Parser_ID;
                           Parameters : Parser_Parameter_access)
                           return EU_Projects.Projects.Project_Descriptor;

   Parsing_Error : exception;
private

   type Parser_ID is new Unbounded_String;

   No_Parser : constant Parser_ID := Parser_ID (Null_Unbounded_String);


   function To_Id (X : String) return Parser_ID
   is (To_Unbounded_String (X));


   overriding function "<" (X, Y : Parser_ID) return Boolean
   is (Unbounded_String (X) < Unbounded_String (Y));

   procedure Register_Extension (Parser         : Parser_ID;
                                 File_Extension : String);

end Project_Processor.Parsers;
