with Rich_Text.Handler_Plugins.Parsers;
with Plugins.Tables;

package Rich_Text.Handler_Plugins.Parser_Tables is
  new Plugins.Tables (Root_Plugin_Type  => Parsers.Parser_Interface,
                      Plugin_Parameters => Parsers.Parser_Parameters,
                      Plugin_ID         => Plugin_ID,
                      Constructor       => Parsers.Create);
