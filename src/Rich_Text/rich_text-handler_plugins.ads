with Ada.Strings.Unbounded;
private
package Rich_Text.Handler_Plugins is

   type Plugin_ID is private;

   No_Plugin : constant Plugin_ID;

   function "<" (X, Y : Plugin_ID) return Boolean;

   function To_Id (X : String) return Plugin_ID;

   function Image (X : Plugin_ID) return String;


private
   use Ada.Strings.Unbounded;

   type Plugin_Id is new Unbounded_String;

   No_Plugin : constant Plugin_Id := Plugin_Id (Null_Unbounded_String);


   function To_Id (X : String) return Plugin_Id
   is (To_Unbounded_String (X));

   function Image (X : Plugin_ID) return String
   is (To_String (X));

   overriding function "<" (X, Y : Plugin_Id) return Boolean
   is (Unbounded_String (X) < Unbounded_String (Y));
end Rich_Text.Handler_Plugins;
