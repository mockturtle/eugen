private
package Rich_Text.Private_Core is
   type Basic_Rich_Text_Type is private;

   Void_Text : constant Basic_Rich_Text_Type;
private
   type Basic_Rich_Text_Type is new Integer;

   Void_Text : constant Basic_Rich_Text_Type := 0;
end Rich_Text.Private_Core;
