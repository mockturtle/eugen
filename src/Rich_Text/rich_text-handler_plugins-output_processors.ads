with Plugins;
with Rich_Text.Private_Core;

package Rich_Text.Handler_Plugins.Output_Processors is
   subtype Processor_Parameters is Plugins.Parameter_Map;

   type Output_Processor_Interface is interface;
   type Output_Processor_access is access all Output_Processor_Interface'class;

   function Create (Params : not null access Processor_Parameters)
                    return Output_Processor_Interface
                    is abstract;

   function Convert (Processor : Output_Processor_Interface;
                     Text      : Private_Core.Basic_Rich_Text_Type)
                     return String
                     is abstract;
end Rich_Text.Handler_Plugins.Output_Processors;
