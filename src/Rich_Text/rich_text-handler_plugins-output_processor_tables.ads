with Rich_Text.Handler_Plugins.Output_Processors;
with Plugins.Tables;

package Rich_Text.Handler_Plugins.Output_Processor_Tables is
  new Plugins.Tables
    (Root_Plugin_Type  => Output_Processors.Output_Processor_Interface,
     Plugin_Parameters => Output_Processors.Processor_Parameters,
     Plugin_ID         => Plugin_ID,
     Constructor       => Output_Processors.Create);
