private with Rich_Text.Handler_Plugins.Parsers;
private with Rich_Text.Handler_Plugins.Output_Processors;
private with Rich_Text.Private_Core;

with Plugins;
with Ada.Strings.Unbounded;

package Rich_Text.Core is
   type Rich_Text_Type is private;

   No_Text : constant Rich_Text_Type;

   function Is_Rich(Item: Rich_Text_Type) return Boolean;

   type Rich_Text_Parser is private;

   Trivial_Parser : constant Rich_Text_Parser;
   -- The trivial parser create a non-rich Rich Text; that is, it
   -- stores just the source.  This allows for using simple texts
   -- transparently

   function Get_Parser
     (Name   : String;
      Params : Plugins.Parameter_Map_Access := Plugins.Empty_Map)
      return Rich_Text_Parser;

   function Parse
     (Input  : String;
      Parser : Rich_Text_Parser := Trivial_Parser)
      return Rich_Text_Type
     with Post => (if Parser = Trivial_Parser then
                     not Is_Rich (Parse'Result));



   type Rich_Text_Formatter is private;

   Trivial_Formatter : constant Rich_Text_Formatter;
   -- A trivial formatter just return the "source" of the Rich Text.



   function Get_Formatter
     (Name : String;
      Params : Plugins.Parameter_Map_Access := Plugins.Empty_Map)
      return Rich_Text_Formatter;

   function Image
     (Item      : Rich_Text_Type;
      Formatter : Rich_Text_Formatter := Trivial_Formatter)
      return String;

   Unknown_Parser : exception;
   Unknown_Formatter : exception;
private
   use Ada.Strings.Unbounded;
   use type Private_Core.Basic_Rich_Text_Type;

   type Rich_Text_Type is
      record
         Content : Private_Core.Basic_Rich_Text_Type;
         Source  : Unbounded_String;
      end record;

   No_Text : constant Rich_Text_Type :=
               Rich_Text_Type'(Content => Private_Core.Void_Text,
                               Source  => Null_Unbounded_String);

   type Rich_Text_Parser is new Handler_Plugins.Parsers.Parser_Access;

   Trivial_Parser : constant Rich_Text_Parser := null;


   type Rich_Text_Formatter is new Handler_Plugins.Output_Processors.Output_Processor_Access;

   Trivial_Formatter : constant Rich_Text_Formatter := null;

   function Is_Rich (Item : Rich_Text_Type) return Boolean
   is (Item.Content /= Private_Core.Void_Text);
end Rich_Text.Core;
