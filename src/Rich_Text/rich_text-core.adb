pragma Ada_2012;
with Rich_Text.Handler_Plugins.Parser_Tables;
with Rich_Text.Handler_Plugins.Output_Processor_Tables;

package body Rich_Text.Core is

   ----------------
   -- Get_Parser --
   ----------------

   function Get_Parser
     (Name : String;
      Params : Plugins.Parameter_Map_Access := Plugins.Empty_Map)
      return Rich_Text_Parser
   is
      use Handler_Plugins;

      Id : constant Plugin_ID := To_Id (Name);
      Tmp : Parsers.Parser_Access;
   begin
      if not Parser_Tables.Exists (Id) then
         raise Unknown_Parser with Name;
      end if;

      Tmp := new Parsers.Parser_Interface'Class'(Parser_Tables.Get (Id, Params));
      return Rich_Text_Parser (Tmp);
   end Get_Parser;

   -----------
   -- Parse --
   -----------

   function Parse
     (Input  : String;
      Parser : Rich_Text_Parser := Trivial_Parser)
      return Rich_Text_Type
   is
   begin
      if Parser = Trivial_Parser then
         return Rich_Text_Type'(Content => Private_Core.Void_Text,
                                Source  => To_Unbounded_String (Input));
      else
         return Rich_Text_Type'(Content => Parser.Parse(Input),
                                Source  => To_Unbounded_String (Input));
      end if;
   end Parse;

   -------------------
   -- Get_Formatter --
   -------------------

   function Get_Formatter
     (Name : String;
      Params : Plugins.Parameter_Map_Access := Plugins.Empty_Map)
      return Rich_Text_Formatter
    is
      use Handler_Plugins;
      use Handler_Plugins.Output_Processors;

      Id : constant Plugin_ID := To_Id (Name);
      Tmp : Output_Processors.Output_Processor_Access;
   begin
      if not Parser_Tables.Exists (Id) then
         raise Unknown_Formatter with Name;
      end if;

      Tmp := new Output_Processor_Interface'Class'(Output_Processor_Tables.Get (Id, Params));
      return Rich_Text_Formatter (Tmp);
  end Get_Formatter;

   -----------
   -- Image --
   -----------

   function Image
     (Item      : Rich_Text_Type;
      Formatter : Rich_Text_Formatter := Trivial_Formatter) return String
   is
   begin
      if Formatter = Trivial_Formatter or not Is_Rich (Item) then
         return To_String (Item.Source);
      else
         return Formatter.Convert (Item.Content);
      end if;
   end Image;

end Rich_Text.Core;
