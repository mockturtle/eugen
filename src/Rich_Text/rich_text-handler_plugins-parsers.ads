with Rich_Text.Private_Core;
with Plugins;

package Rich_Text.Handler_Plugins.Parsers is
   subtype Parser_Parameters is Plugins.Parameter_Maps.Map;

   type Parser_Interface is interface;

   type Parser_Access is access all Parser_Interface'Class;

   function Create (Params : not null access Parser_Parameters)
                    return Parser_Interface
                    is abstract;

   function Parse (Parser : in out Parser_Interface;
                   Input  : String)
                   return Private_Core.Basic_Rich_Text_Type
                   is abstract;
end Rich_Text.Handler_Plugins.Parsers;
