package Rich_Text  is
   --
   -- This hierarchy of packages allows to handle a "rich text" of the
   -- type of markdown.  This package is divided into three large blocks
   --
   --   * A "core" that defines a type representing a rich text together
   --     subroutines to create and access it
   --
   --   * A "parser interface" that defines the abstract version of
   --     parsers used to bring an external description into the internal
   --     form (e.g., a markdown-to-internal parser)
   --
   --   * An "output processor" that takes the internal form and transforms
   --     it into an external rich-text-format (e.g., internal-to-latex
   --     processor)
   --
   --
   --
end Rich_Text;
