with Ada.Containers.Vectors;
with Line_Parsers.Receivers.Multi_Receivers;
with Utilities;
with Plugins;
with Ada.Directories;
--  with Ada.Command_Line;
--  with Ada.Strings.Maps;
--  with Ada.Characters.Handling; use Ada.Characters.Handling;
with Ada.Text_IO;
with Ada.Exceptions;
with Ada.IO_Exceptions;

package body Project_Processor.Configuration is
   package Call_Lists is
     new Ada.Containers.Vectors (Index_Type   => Positive,
                                 Element_Type => Processing_Call);

   package Multi_String_Receivers is
     new Line_Parsers.Receivers.Multi_Receivers (Line_Parsers.Receivers.String_Receiver);


   Input_File   : aliased Line_Parsers.Receivers.String_Receiver;
   Format       : aliased Line_Parsers.Receivers.String_Receiver;
   Processors   : aliased Multi_String_Receivers.Multi_Receiver;
   Help_Flag    : aliased Line_Parsers.Receivers.Boolean_Receiver;

   Parser_Format : Parsers.Parser_ID := Parsers.No_Parser;
   Parser_Param  : Plugins.Parameter_Map_Access :=
                     new Plugins.Parameter_Map'(Plugins.Parameter_Maps.Empty_Map);

--     package Help_Flag is
--        procedure Set (Value : Boolean);
--        function Is_Set return Boolean;
--     end Help_Flag;
--
--     package body Help_Flag is
--        Flag : Boolean;
--        Init : Boolean := False;
--
--        procedure Set (Value : Boolean) is
--        begin
--           if Init then
--              raise Program_Error with "BUG: Help flag set twice";
--           end if;
--
--           Init := True;
--           Flag := Value;
--        end Set;
--
--        function Is_Set return Boolean is
--        begin
--           if not Init then
--              raise Program_Error with "BUG: Help flag read before write";
--           end if;
--
--           return Flag;
--        end Is_Set;
--     end Help_Flag;

   function Help_Required return Boolean
   is (Help_Flag.Is_Set);

   ----------------
   -- Initialize --
   ----------------

   procedure Initialize is
      use Line_Parsers;
--        use Ada.Command_Line;

      Parser : Line_Parser := Create (Case_Sensitive => False);

--        Config_Lines : String_Vectors.Vector := String_Vectors.Empty_Vector;
--
--        procedure Load_Config_Data is
--        begin
--           if Argument_Count = 1 then
--              declare
--                 Arg : constant String := Argument (1);
--              begin
--                 if Arg (Arg'First) = '@' then
--                    Config_Lines := Slurp (Arg (Arg'First + 1 .. Arg'Last));
--                 end if;
--              end;
--           end if;
--
--           if Config_Lines.Is_Empty then
--              Config_Lines := Slurp (Default_Config_Filename);
--           end if;
--        end Load_Config_Data;

      procedure Parse_Parser_Specs is
        new Plugins.Parse_Plugin_Specs (Plugin_ID => Parsers.Parser_ID,
                                        To_Name   => Parsers.To_Id);

--        procedure Check_If_Help_Asked is
--           function Trim_Minus (X : String) return String
--           is
--              use Ada.Strings.Fixed;
--              Pos : constant Natural := Index (Source => X,
--                                               Set    => Ada.Strings.Maps.To_Set ('-'),
--                                               Test   => Ada.Strings.Outside);
--           begin
--              if Pos = 0 then
--                 return "";
--              else
--                 return X (Pos .. X'Last);
--              end if;
--           end Trim_Minus;
--        begin
--           if Argument_Count = 0 then
--              if Config_Lines.Is_Empty then
--                 Help_Flag.Set (True);
--              else
--                 Help_Flag.Set (False);
--              end if;
--
--           elsif Argument_Count = 1 then
--              declare
--                 Arg : constant String := Trim_Minus (To_Lower (Argument (1)));
--              begin
--                 Help_Flag.Set (Arg = "h" or  Arg = "help" or Arg = "?");
--              end;
--
--           else
--              Help_Flag.Set (False);
--           end if;
--        end Check_If_Help_Asked;
   begin
      Add_Parameter (Parser,
                     Name       => "in,input",
                     If_Missing => Die,
                     Default    => "",
                     Handler    => Input_File'Access);

      Add_Parameter (Parser,
                     Name       => "p,proc,processor",
                     If_Missing => Ignore,
                     Default    => "",
                     Handler    => Processors'Access);

      Add_Parameter (Parser,
                     Name       => "h,help,?,-h,-help,--help,--h",
                     If_Missing => Ignore,
                     Default    => "",
                     Handler    => Help_Flag'Access);


      Add_Parameter (Parser,
                     Name       => "f,fmt,format",
                     If_Missing => Ignore,
                     Default    => "",
                     Handler    => Format'Access);

      Parse_Command_Line (Parser,
                          On_Empty_Line => "help",
                          Help_Flag => Help_Flag'access);

      if Help_Flag.Get then
         return;
      end if;

      if Format.Is_Set then
         Parse_Parser_Specs (Input       => Format.Value,
                             Plugin_Name => Parser_Format,
                             Parameters  => Parser_Param.all);
      else
         Parser_Format := Parsers.No_Parser;
         Parser_Param  := Plugins.Empty_Map;
      end if;
   exception
      when Error : Line_Parsers.Bad_Command =>
         raise Configuration.Bad_Command_Line
         with Ada.Exceptions.Exception_Message (Error);

   end Initialize;



   -------------------
   -- For_All_Calls --
   -------------------



   procedure For_All_Calls
     (Callback : not null access procedure (Call : Processing_Call))
   is
      use Project_Processor.Processors;

      procedure Parse is
        new Plugins.Parse_Plugin_Specs (Plugin_ID => Processor_ID,
                                        To_Name   => To_Id);
      Data : Processing_Call := (Processing_Call'(Name       => <>,
                                                  Parameters => new Plugins.Parameter_Maps.Map));
   begin
      for Str of Processors.All_Values loop
         Parse (Input       => Str.Value,
                Plugin_Name => Data.Name,
                Parameters  => Data.Parameters.all);

         Callback (Data);
      end loop;
   end For_All_Calls;

   ----------------
   -- Input_Data --
   ----------------

   function Input_Data return String is
   begin
      return Utilities.Slurp (Input_File.Value);
   exception
      when Ada.IO_Exceptions.Name_Error =>
         raise Input_Not_Found with Input_File.Value;
   end Input_Data;


   ------------------
   -- Input_Format --
   ------------------

   function Input_Format return Parsers.Parser_ID
   is
      function Guess (Filename : String) return Parsers.Parser_ID
      is
         use Ada.Directories;
         use Parsers;

         Ext    : constant String := Extension (Filename);
         Result : constant Parser_ID := Find_Parser (Ext);
      begin
         if Result = No_Parser then
            raise Bad_Command_Line with "Unknown format";
         else
            return Result;
         end if;
      end Guess;

      use type Parsers.Parser_ID;
   begin
      if Parser_Format /= Parsers.No_Parser then
         return Parser_Format;

      else
         return Guess (Input_File.Value);

      end if;
   end Input_Format;

   -----------------------
   -- Parser_Parameters --
   -----------------------

   function Parser_Parameters
     return Parsers.Parser_Parameter_Access
   is
   begin
      return Parser_Param;
   end Parser_Parameters;


   procedure Print_Help is
      procedure P (X : String := "") is
      begin
         Ada.Text_IO.Put_Line (Ada.Text_IO.Standard_Error, X);
      end P;
   begin
      P;
      P ("Usage: eugen in=input_file [fmt=format_spec] p=processor_spec...");
      P;
      P ("format and processor specs have the format");
      P;
      P ("   <spec>      := <plugin_name>[:<parameter>(;<parameter>)*]");
      P ("   <parameter> := <name>[=<value>]");
      P;
      P ("Example:");
      P ("   latex:show-deliv;deliv-color=yellow");
      P;
   end Print_Help;


end Project_Processor.Configuration;


